﻿SET IDENTITY_INSERT [dbo].[Author] ON;

DECLARE @JohnId INT = 1;
IF NOT EXISTS (SELECT [Id] FROM [dbo].[Author] WHERE [Id] = @JohnId)
BEGIN

  INSERT INTO [dbo].[Author] ([Id],[FullName])
  VALUES (@JohnId, N'John Smith')

END

DECLARE @VictorId INT = 2;
IF NOT EXISTS (SELECT [Id] FROM [dbo].[Author] WHERE [Id] = @VictorId)
BEGIN

  INSERT INTO [dbo].[Author] ([Id],[FullName])
  VALUES (@VictorId, N'Victor Norris')

END

DECLARE @ArthurId INT = 3;
IF NOT EXISTS (SELECT [Id] FROM [dbo].[Author] WHERE [Id] = @ArthurId)
BEGIN

  INSERT INTO [dbo].[Author] ([Id],[FullName])
  VALUES (@ArthurId, N'Arthur Miller')

END

DECLARE @AnonymousId INT = 4;
IF NOT EXISTS (SELECT [Id] FROM [dbo].[Author] WHERE [Id] = @AnonymousId)
BEGIN

  INSERT INTO [dbo].[Author] ([Id],[FullName])
  VALUES (@AnonymousId, N'Anonymous')

END

DECLARE @UnknownAuthorId INT = 5;
IF NOT EXISTS (SELECT [Id] FROM [dbo].[Author] WHERE [Id] = @UnknownAuthorId)
BEGIN

  INSERT INTO [dbo].[Author] ([Id],[FullName])
  VALUES (@UnknownAuthorId, N'Unknown Author')

END

SET IDENTITY_INSERT [dbo].[Author] OFF;