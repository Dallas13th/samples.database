﻿SET IDENTITY_INSERT [dbo].[DataTypesTestTable] ON;

DECLARE @firstId INT = 1;
IF NOT EXISTS (SELECT [Id] FROM [dbo].[DataTypesTestTable] WHERE [Id] = @firstId)
BEGIN

  INSERT INTO [dbo].[DataTypesTestTable] ([Id],[DateTimeOffsetValue])
  VALUES (@firstId, SYSDATETIMEOFFSET())

END

DECLARE @secondId INT = 2;
IF NOT EXISTS (SELECT [Id] FROM [dbo].[DataTypesTestTable] WHERE [Id] = @secondId)
BEGIN

  INSERT INTO [dbo].[DataTypesTestTable] ([Id],[DateTimeOffsetValue])
  VALUES (@secondId, SYSDATETIMEOFFSET())

END

DECLARE @thrirdId INT = 3;
IF NOT EXISTS (SELECT [Id] FROM [dbo].[DataTypesTestTable] WHERE [Id] = @thrirdId)
BEGIN

  INSERT INTO [dbo].[DataTypesTestTable] ([Id],[DateTimeOffsetValue])
  VALUES (@thrirdId, SYSDATETIMEOFFSET())

END

SET IDENTITY_INSERT [dbo].[DataTypesTestTable] OFF;