﻿SET IDENTITY_INSERT [dbo].[Book] ON;

DECLARE @ComedyBookId INT = 1;
IF NOT EXISTS (SELECT [Id] FROM [dbo].[Book] WHERE [Id] = @ComedyBookId)
BEGIN

  INSERT INTO [dbo].[Book] ([Id], [AuthorId], [GenreId], [Name], [ReleaseDate])
  VALUES (@ComedyBookId, 1, 1, N'Funny Book', GETDATE())

END

DECLARE @FantasyBookId INT = 2;
IF NOT EXISTS (SELECT [Id] FROM [dbo].[Book] WHERE [Id] = @FantasyBookId)
BEGIN

  INSERT INTO [dbo].[Book] ([Id], [AuthorId], [GenreId], [Name], [ReleaseDate])
  VALUES (@FantasyBookId, 2, 2, N'Fantasy Book', GETDATE())

END

DECLARE @HorrorBookId INT = 3;
IF NOT EXISTS (SELECT [Id] FROM [dbo].[Book] WHERE [Id] = @HorrorBookId)
BEGIN

  INSERT INTO [dbo].[Book] ([Id], [AuthorId], [GenreId], [Name], [ReleaseDate])
  VALUES (@HorrorBookId, 3, 3, N'Scareful Book', GETDATE())

END

DECLARE @MixedBookId INT = 4;
IF NOT EXISTS (SELECT [Id] FROM [dbo].[Book] WHERE [Id] = @MixedBookId)
BEGIN

  INSERT INTO [dbo].[Book] ([Id], [AuthorId], [GenreId], [Name], [ReleaseDate])
  VALUES (@MixedBookId, 4, 4, N'Mixed Book', GETDATE())

END

DECLARE @UnknownBookId INT = 5;
IF NOT EXISTS (SELECT [Id] FROM [dbo].[Book] WHERE [Id] = @UnknownBookId)
BEGIN

  INSERT INTO [dbo].[Book] ([Id], [AuthorId], [GenreId], [Name], [ReleaseDate])
  VALUES (@UnknownBookId, 5, 5, N'Unknown Book', GETDATE())

END

SET IDENTITY_INSERT [dbo].[Book] OFF;