﻿SET IDENTITY_INSERT [dbo].[Genre] ON;

DECLARE @ComedyId INT = 1;
IF NOT EXISTS (SELECT [Id] FROM [dbo].[Genre] WHERE [Id] = @ComedyId)
BEGIN

  INSERT INTO [dbo].[Genre] ([Id],[Name])
  VALUES (@ComedyId, N'Comedy')

END

DECLARE @FantasyId INT = 2;
IF NOT EXISTS (SELECT [Id] FROM [dbo].[Genre] WHERE [Id] = @FantasyId)
BEGIN

  INSERT INTO [dbo].[Genre] ([Id],[Name])
  VALUES (@FantasyId, N'Fantasy')

END

DECLARE @HorrorId INT = 3;
IF NOT EXISTS (SELECT [Id] FROM [dbo].[Genre] WHERE [Id] = @HorrorId)
BEGIN

  INSERT INTO [dbo].[Genre] ([Id],[Name])
  VALUES (@HorrorId, N'Horror')

END

DECLARE @MixedId INT = 4;
IF NOT EXISTS (SELECT [Id] FROM [dbo].[Genre] WHERE [Id] = @MixedId)
BEGIN

  INSERT INTO [dbo].[Genre] ([Id],[Name])
  VALUES (@MixedId, N'Mixed')

END

DECLARE @UnknownGenreId INT = 5;
IF NOT EXISTS (SELECT [Id] FROM [dbo].[Genre] WHERE [Id] = @UnknownGenreId)
BEGIN

  INSERT INTO [dbo].[Genre] ([Id],[Name])
  VALUES (@UnknownGenreId, N'Unknown Genre')

END

SET IDENTITY_INSERT [dbo].[Genre] OFF;