﻿CREATE PROCEDURE [dbo].[GetFragmentedIndexesProdedure]
	@TableNames StringTableType READONLY,
	@MinAvgFragPercents DECIMAL(8, 2)
AS
BEGIN
	SELECT OBJECT_NAME(ind.object_id) AS TableName,
       ind.name AS IndexName,
       indexstats.index_type_desc AS IndexType,
       indexstats.avg_fragmentation_in_percent AS AverageFragmentationInPercents
	FROM sys.dm_db_index_physical_stats(DB_ID(), NULL, NULL, NULL, NULL) indexstats
		INNER JOIN sys.indexes ind
			ON ind.object_id = indexstats.object_id
			   AND ind.index_id = indexstats.index_id
	WHERE indexstats.avg_fragmentation_in_percent > @MinAvgFragPercents
		  AND EXISTS
	(
		SELECT 1 FROM @TableNames WHERE StringValue = OBJECT_NAME(ind.object_id)
	)
	ORDER BY indexstats.avg_fragmentation_in_percent DESC;
END
