﻿CREATE PROCEDURE [dbo].[EchoTvpProcedure]
	@TableParameter CommonTableType READONLY
AS
BEGIN
	SELECT
	GuidValue,
	IntValue,
	StringValue,
	DateTimeValue,
	DecimalValue,
	MoneyValue,
	DateTimeOffsetValue
	FROM @TableParameter tvp
END