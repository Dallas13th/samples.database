﻿CREATE PROCEDURE [dbo].[GetBooksProcedure]
AS
BEGIN
	SELECT
	b.Id AS BookId,
	a.[FullName] AS AuthorName,
	g.[Name] AS GenreName,
	b.[Name] AS BookName,
	b.[ReleaseDate] AS ReleaseDate
	FROM [dbo].[Book] b
	JOIN [dbo].[Author] a ON b.AuthorId = a.Id
	JOIN [dbo].[Genre] g ON b.GenreId = g.Id
END