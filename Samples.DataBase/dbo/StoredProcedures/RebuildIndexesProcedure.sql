﻿CREATE PROCEDURE [dbo].[RebuildIndexesProcedure]
	@TableNames StringTableType READONLY,
	@MinAvgFragPercents DECIMAL(8, 2)
AS
BEGIN
	DECLARE @TableName NVARCHAR(255);
	DECLARE @IndexName NVARCHAR(MAX);
	DECLARE @IndexType NVARCHAR(255);
	DECLARE @SqlString NVARCHAR(MAX);
	DECLARE @SqlPostfix NVARCHAR(255);
	DECLARE @AverageFragmentationInPercents INT;
	DECLARE TableCursor CURSOR FORWARD_ONLY READ_ONLY LOCAL FOR
	SELECT OBJECT_NAME(ind.object_id) AS TableName,
		   ind.name AS IndexName,
		   indexstats.index_type_desc AS IndexType,
		   indexstats.avg_fragmentation_in_percent AS AverageFragmentationInPercents
	FROM sys.dm_db_index_physical_stats(DB_ID(), NULL, NULL, NULL, NULL) indexstats
		INNER JOIN sys.indexes ind
			ON ind.object_id = indexstats.object_id
			   AND ind.index_id = indexstats.index_id
	WHERE indexstats.avg_fragmentation_in_percent > @MinAvgFragPercents
		  AND EXISTS
	(
		SELECT 1 FROM @TableNames WHERE StringValue = OBJECT_NAME(ind.object_id)
	)
	ORDER BY indexstats.avg_fragmentation_in_percent DESC;
	OPEN TableCursor;
	FETCH NEXT FROM TableCursor
	INTO @TableName,
		 @IndexName,
		 @IndexType,
		 @AverageFragmentationInPercents;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF (@AverageFragmentationInPercents >= 30)
		BEGIN
			PRINT N'Running REBUILD for ' + @IndexName + N' of ' + @TableName + N' with avgFragPercent '
				  + CAST(@AverageFragmentationInPercents AS NVARCHAR(255));

			SET @SqlPostfix = N' REBUILD;'; -- WITH (ONLINE = ON) --enterprise
		END;
		ELSE
		BEGIN
			PRINT N'Running REORGANIZE for ' + @IndexName + N' of ' + @TableName + N' with avgFragPercent '
				  + CAST(@AverageFragmentationInPercents AS NVARCHAR(255));

			SET @SqlPostfix = N' REORGANIZE;';
		END;

		SET @SqlString = N'ALTER INDEX ' + @IndexName + N' ON ' + @TableName + @SqlPostfix;

		EXEC sp_executesql @SqlString;

		FETCH NEXT FROM TableCursor
		INTO @TableName,
			 @IndexName,
			 @IndexType,
			 @AverageFragmentationInPercents;
	END;
	CLOSE TableCursor;
	DEALLOCATE TableCursor;
END