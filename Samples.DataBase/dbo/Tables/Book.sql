﻿CREATE TABLE [dbo].[Book]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [AuthorId] INT NOT NULL, 
    [GenreId] INT NOT NULL, 
    [Name] NVARCHAR(150) NOT NULL, 
    [ReleaseDate] DATETIME2 NOT NULL,

	CONSTRAINT [FK_Book_GenreId__Genre_Id] FOREIGN KEY (GenreId) REFERENCES [Genre] ([Id]),
	CONSTRAINT [FK_Book_AuthorId__Author_Id] FOREIGN KEY (AuthorId) REFERENCES [Author] ([Id])
)