﻿CREATE TABLE [dbo].[DataTypesTestTable]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [DateTimeOffsetValue] DATETIMEOFFSET NOT NULL, 
    [RowVersion] ROWVERSION NOT NULL
)
