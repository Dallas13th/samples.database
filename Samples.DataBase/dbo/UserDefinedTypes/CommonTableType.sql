﻿CREATE TYPE [dbo].[CommonTableType] AS TABLE
(
    [GuidValue] UNIQUEIDENTIFIER NOT NULL,
    [IntValue] INT NOT NULL,
    [StringValue] NVARCHAR (MAX) NOT NULL,
    [DateTimeValue] DATETIME2 (7) NOT NULL,
    [DecimalValue] DECIMAL (18) NOT NULL,
    [MoneyValue] MONEY NOT NULL,
	[DateTimeOffsetValue] DATETIMEOFFSET NOT NULL,
    PRIMARY KEY CLUSTERED ([GuidValue] ASC)
);