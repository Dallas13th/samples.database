﻿CREATE TYPE [dbo].[StringTableType] AS TABLE
(
    [StringValue] NVARCHAR (MAX) NOT NULL
);