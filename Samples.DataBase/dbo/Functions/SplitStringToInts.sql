﻿CREATE FUNCTION [dbo].[SplitStringToInts]
(
	@StringParam NVARCHAR(MAX),
	@Separator NCHAR = ','
)
RETURNS @resultTable TABLE
(
	IntValue INT NOT NULL,
	StringValue NVARCHAR(10) NOT NULL
)
AS
BEGIN
	INSERT INTO @resultTable (IntValue, StringValue)
	SELECT
		CONVERT(INT, value) AS IntValue,
		value AS StringValue
	FROM STRING_SPLIT(@StringParam, @Separator)
	RETURN;
END