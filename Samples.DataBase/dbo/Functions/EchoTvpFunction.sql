﻿CREATE FUNCTION [dbo].[EchoTvpFunction]
(
	@TableParameter CommonTableType READONLY
)
RETURNS @returntable TABLE
(
	GuidValue UNIQUEIDENTIFIER PRIMARY KEY NOT NULL,
	IntValue INT NOT NULL,
	StringValue NVARCHAR(MAX) NOT NULL,
	DateTimeValue DATETIME2(7) NOT NULL,
	DecimalValue DECIMAL NOT NULL,
	MoneyValue MONEY NOT NULL,
	DateTimeOffsetValue DATETIMEOFFSET NOT NULL
)
AS
BEGIN
	INSERT INTO @returntable
	(
		GuidValue,
		IntValue,
		StringValue,
		DateTimeValue,
		DecimalValue,
		MoneyValue,
		DateTimeOffsetValue
	)
	SELECT
		GuidValue,
		IntValue,
		StringValue,
		DateTimeValue,
		DecimalValue,
		MoneyValue,
		DateTimeOffsetValue
	FROM @TableParameter
	RETURN;
END