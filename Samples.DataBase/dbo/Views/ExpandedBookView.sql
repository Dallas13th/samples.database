﻿CREATE VIEW [dbo].[ExpandedBookView] WITH SCHEMABINDING
AS
SELECT
	B.Id AS BookId,
	B.[Name] AS [Book],
	A.[FullName] AS [Author],
	G.[Name] AS [Genre],
	B.[ReleaseDate]
FROM [dbo].[Book] B
JOIN [dbo].[Author] A ON B.[AuthorId] = A.[Id]
JOIN [dbo].[Genre] G ON B.[GenreId] = G.[Id]
GO
CREATE UNIQUE CLUSTERED INDEX [CIDX_ExpandedBookView]
    ON [dbo].[ExpandedBookView]([BookId] ASC) WITH (STATISTICS_NORECOMPUTE = ON);