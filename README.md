# README #

# For project compilation make sure you have installed:
* Latest [Visual studio 2019](https://www.visualstudio.com/)
* Latest [.Net Framework SDK](https://www.microsoft.com/net/download/visual-studio-sdks)
* [MSSQL 2017](https://www.microsoft.com/ru-ru/sql-server/sql-server-downloads)

# For db management:
* Any DBMS (for example [SSMS](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms))